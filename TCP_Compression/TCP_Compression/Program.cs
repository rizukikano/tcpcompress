﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;

namespace TCP_Compression
{
    class Program
    {
        /*
         * TCP SERVER
         * */
        private static string directoryPath = @"C:\Users\Nitro 5\Pictures";
        private static string fileName = "C:\\Users\\Nitro 5\\Pictures\\Black Clover - 47 [720p].mkv.gz";
        private static int port = 10000;
        private static FileInfo info;
        private static FileStream compressedFileStream;
        private static FileStream originalFileStream;

        static void Main(string[] args)
        {
            DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
            Compress(directorySelected);
            Console.WriteLine("Press enter to start sending file");
            Console.ReadKey();
            SendFiles();
        }

        public static void SendFiles()
        {
            try
            {
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");

                TcpListener listener = new TcpListener(ipAddress, port);

                listener.Start();

                Console.WriteLine("Waiting for a connection.....");

                Socket sock = listener.AcceptSocket();
                Console.WriteLine("Connection accepted from " + sock.RemoteEndPoint);
              
                sock.SendFile(fileName);

                sock.Close();
                listener.Stop();

                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }

        public static string checkMD5(string fileName)
        {
            var stream = File.OpenRead(fileName);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(stream);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            stream.Close();
            return sb.ToString();
        }

        public static void Compress(DirectoryInfo directorySelected)
        {
            foreach (FileInfo fileToCompress in directorySelected.GetFiles())
            {
                using (originalFileStream = fileToCompress.OpenRead())
                {
                    if ((File.GetAttributes(fileToCompress.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden &
                        fileToCompress.Extension != ".gz")
                    {
                        Console.WriteLine("MD5 before compressed = {0}", checkMD5(originalFileStream.Name));
                        using (compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                        {
                            using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                                CompressionMode.Compress, false))
                            {
                                originalFileStream.CopyTo(compressionStream);
                            }
                            Console.WriteLine("MD5 after compressed = {0}", checkMD5(compressedFileStream.Name));
                        }
                        info = new FileInfo(directoryPath + "\\" + fileToCompress.Name + ".gz");
                        Console.WriteLine("Compressed {0} from {1} to {2} bytes.", fileToCompress.Name,
                        fileToCompress.Length, info.Length.ToString());
                    }
                }
            }
        }
    }
}

