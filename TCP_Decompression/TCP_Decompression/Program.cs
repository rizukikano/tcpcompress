﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;

namespace TCP_Decompression
{
    class Program
    {
        /*
         * TCP CLIENT
         * */
        private static int port = 10000;
        private static string directoryPath = "C:\\Users\\Nitro 5\\Documents\\Tugas\\Pemrograman Jaringan";
        static void Main(string[] args)
        {
            Console.WriteLine("Press enter to start receiving file");
            Console.ReadKey();
            ReceiveFiles();
            Console.ReadKey();
            DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
            foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
            {
                Decompress(fileToDecompress);
            }
            Console.ReadKey();
        }

        public static void ReceiveFiles()
        {
            try
            {
                TcpClient tcpclnt = new TcpClient();
                Console.WriteLine("Connecting.....");

                tcpclnt.Connect("127.0.0.1", port);

                Console.WriteLine("Connected");
                
                Stream stream = tcpclnt.GetStream();
                
                Console.WriteLine("Receiving File.....");
                
                while (true)
                {
                    using (var output = File.Create("C:\\Users\\Nitro 5\\Documents\\Tugas\\Pemrograman Jaringan\\Hasil.gz"))
                    {
                        Console.WriteLine("Client connected. Starting to receive the file");
                        
                        var buffer = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                        }
                        Console.WriteLine("File Received!");
                        break;
                    }
                }
                tcpclnt.Close();
                Console.ReadKey();
            }

            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }

        public static string checkMD5(string fileName)
        {
            var stream = File.OpenRead(fileName);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(stream);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            stream.Close();
            return sb.ToString();
        }

        public static void Decompress(FileInfo fileToDecompress)
        {
            FileStream originalFileStream;
            FileStream decompressedFileStream;
            GZipStream decompressionStream;
            using (originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);
                using (decompressedFileStream = File.Create(newFileName + ".jpg"))
                {
                    Console.WriteLine("MD5 before decompressed = {0}", checkMD5(originalFileStream.Name));
                    using (decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress, false))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed {0}", fileToDecompress.Name);
                    }
                }
            }
            Console.WriteLine("MD5 after decompressed = {0}", checkMD5(decompressedFileStream.Name));
        }
    }
}
